### Titre du livre

Le résumé: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin faucibus 
ligula sit amet felis mollis scelerisque. Fusce urna metus, finibus eu posuere vitae, 
elementum et arcu. Donec accumsan, nunc sed mollis fermentum, enim nulla efficitur urna, 
eget convallis tortor neque dapibus lectus. Phasellus placerat orci vitae nisi porttitor 
ultrices eget ac tortor. Nulla porttitor nisi sit amet lobortis accumsan. Praesent nec 
fringilla nibh, tempus vehicula quam. Fusce ut ante massa. Fusce volutpat cursus libero id pretium. 

Mon avis: 4/5

### Titre d'un autre livre

Le résumé: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin faucibus 
ligula sit amet felis mollis scelerisque. Fusce urna metus, finibus eu posuere vitae, 
elementum et arcu. Donec accumsan, nunc sed mollis fermentum, enim nulla efficitur urna, 
eget convallis tortor neque dapibus lectus. Phasellus placerat orci vitae nisi porttitor 
ultrices eget ac tortor. Nulla porttitor nisi sit amet lobortis accumsan. Praesent nec 
fringilla nibh, tempus vehicula quam. Fusce ut ante massa. Fusce volutpat cursus libero id pretium. 

Mon avis: 4/5